# usage:
#   python -m unittest discover
#
from unittest import TestCase

from time_monotonic import time_monotonic
import time


class MonotonicTest(TestCase):

    def test_forward(self):
        a = time_monotonic()
        b = time_monotonic()
        c = time_monotonic()
        self.assertTrue(c >= b >= a)

        a = time_monotonic()
        time.sleep(0.1)
        b = time_monotonic()
        time.sleep(0.1)
        c = time_monotonic()
        self.assertTrue(c > b > a)

    def test_duration(self):
        """ test that the unit really is in seconds
        """
        EPSILON = 0.01
        a = time_monotonic()
        time.sleep(1)
        b = time_monotonic()

        delta = b - a
        self.assertTrue(abs(delta - 1) < EPSILON)

        a = time_monotonic()
        time.sleep(0.1)
        b = time_monotonic()

        delta = b - a
        self.assertTrue(abs(delta - 0.1) < EPSILON)

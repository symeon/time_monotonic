=====
Intro
=====

Backport of python3 ``time.monotonic()`` to python 2.x.

Whenever you have to calculate timeouts you cannot rely on wallclock time. Every usage of ``time.time`` is a bug. 

Use monotonic time instead. This garanteed goes forwards and is not influenced by NTP, timezones, changing time etc.


usage
=====

.. code-block:: python

    from time_monotonic import time_monotonic

    t = time_monotonic()   #  in seconds (float)



installation
============


You can install it using pip::

    pip install git+https://bitbucket.org/Intemo/time_monotonic/#egg=time-monotonic

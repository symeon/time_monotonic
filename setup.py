#!/usr/bin/env python

from distutils.core import setup

setup(name='time_monotonic',
      version='1.0',
      description='Backport of python3 time.monotonic',
      long_description='Backport of python3 time.monotonic functionality. '
                       'Original sources from asyncio project',
      author='Harm Verhagen',
      author_email='harm@symeon.nl',
      packages=['time_monotonic', ],
     )
